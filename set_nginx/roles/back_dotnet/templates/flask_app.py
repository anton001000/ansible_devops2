import os
import shutil
import subprocess
import zipfile

from flask import Flask
from flask import request, jsonify, make_response


FLASK_HOST = "0.0.0.0"
FLASK_PORT = 41337

IP_GREEN_MIG_GROUP = "10.132"
IP_BLUE_MIG_GROUP = "10.154"

BACK_IP = "10.128.0.14:228"
HOMEDIR = "/home/front/dist/"
PATH_TO_DOWNLOAD = "/tmp/flask-test/"

GREEN_ZIP_FILE = PATH_TO_DOWNLOAD + BACK_IP + "/dist.zip"
BLUE_ZIP_FILE = PATH_TO_DOWNLOAD + BACK_IP + "/dist-blue.zip"

DEFAULT_FILE = PATH_TO_DOWNLOAD + BACK_IP + "/default"
DEST_NGINX = "/etc/nginx/sites-available/"


def delete_files_or_directories(path):
    try:
        # bash rm -rf
        shutil.rmtree(path)
    except FileNotFoundError:
        pass


def mkdir_with_script(path):
    try:
        # bash mkdir
        os.mkdir(path, mode=0o777, dir_fd=None)
    except FileExistsError:
        pass


def unzip_files(path_to_in_file, path_to_out):
    # bash unzip
    with zipfile.ZipFile(path_to_in_file, 'r') as zip_ref:
        zip_ref.extractall(path_to_out)


def blue_or_green_branch(mig):
    if mig == "Green":
        return GREEN_ZIP_FILE
    else:
        return BLUE_ZIP_FILE


def update_static(mig):
    delete_files_or_directories(path=PATH_TO_DOWNLOAD)
    mkdir_with_script(path=PATH_TO_DOWNLOAD)

    # download zip files from back
    subprocess.run(["wget", "-r", "-np", "-R", "index.html*", BACK_IP, "-P", PATH_TO_DOWNLOAD])

    mkdir_with_script(path=HOMEDIR)
    delete_files_or_directories(path=HOMEDIR)

    zip_file_blue_or_green = blue_or_green_branch(mig)
    unzip_files(path_to_in_file=zip_file_blue_or_green, path_to_out=HOMEDIR)

    shutil.move(src=DEFAULT_FILE, dst=DEST_NGINX + "default")
    subprocess.run(["systemctl", "restart", "nginx"])


def startup_script():
    my_ip = subprocess.getoutput("hostname -I").split()[0]
    if IP_GREEN_MIG_GROUP in my_ip:
        update_static(mig="Green")
    elif IP_BLUE_MIG_GROUP in my_ip:
        update_static(mig="Blue")


app = Flask(__name__)


@app.route("/", methods=["POST"])
def hello():
    try:
        action = request.values.get("action").rstrip()
        mig = request.values.get("mig").rstrip()

        if action == "update":
            update_static(mig=mig)

            return make_response(jsonify({"message": "Front has updated"}), 200)

        return make_response(jsonify({"message": "The request should have an action in params (action=update)"}), 400)

    except Exception as error:
        return make_response(jsonify({"message": "Internal Server Error", "Error": str(error)}), 500)


if __name__ == "__main__":
    startup_script()

    app.run(host=FLASK_HOST, port=FLASK_PORT)


