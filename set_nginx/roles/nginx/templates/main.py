import re
import shutil
import subprocess

from flask import Flask
from flask import request


app = Flask(__name__)

DESTINATION_DIRECTORY_DEFAULT_FILE = "/etc/nginx/sites-available/"
NGINX_DEFAULT_FILE = "/etc/nginx/sites-available/defaults"
NGINX_DEFAULT_DIRECTORY = "/etc/nginx/sites-available/"
SAVE_FILE = "tmp/"


def get_all_ip_addr_from_default_nginx():
    with open(NGINX_DEFAULT_FILE, "r") as file:
        buffer = file.readline()
        pattern = r"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}"
        return re.findall(pattern, buffer)


def make_upstream_pattern_for_add(list_of_ip_addr):
    upstream_pattern = "upstream web_backend { "
    for ip_addr in list_of_ip_addr:
        instance = "server {ip_addr}; ".format(ip_addr=ip_addr)
        upstream_pattern += instance
    upstream_pattern += " }"

    return upstream_pattern


def make_upstream_pattern_for_delete(list_of_ip_addr, ip_addr):
    upstream_pattern = "upstream web_backend { "

    for ip in list_of_ip_addr:
        if ip != ip_addr:
            instance = "server {ip_addr}; ".format(ip_addr=ip)
            upstream_pattern += instance
    upstream_pattern += " }"

    return upstream_pattern


def replace_upstream(upstream):
    with open(NGINX_DEFAULT_FILE, "r") as file:
        buffer = file.read()
        new_file_buffer = re.sub(r'upstream\sweb_backend\s\{.*\}', upstream, buffer)
        return new_file_buffer


def make_new_default_file(new_file_buffer):
    with open(SAVE_FILE + "defaults", "w") as file:
        file.write(new_file_buffer)




@app.route("/", methods=["POST"])
def hello():
    ip_addr = request.values.get("ip").rstrip()
    action = request.values.get("action").rstrip()

     # Add section
    if action == "add":
        list_of_ip_addr = get_all_ip_addr_from_default_nginx()
        list_of_ip_addr.append(ip_addr)
        upstream_pattern = make_upstream_pattern_for_add(list_of_ip_addr)
        new_file_buffer = replace_upstream(upstream_pattern)
        make_new_default_file(new_file_buffer)

        shutil.move(SAVE_FILE + "defaults", NGINX_DEFAULT_DIRECTORY + "defaults")
        subprocess.run(["nginx", "-s",  "reload"])

    # Delete section
    elif action == "del":
        list_of_ip_addr = get_all_ip_addr_from_default_nginx()
        upstream_pattern = make_upstream_pattern_for_delete(list_of_ip_addr=list_of_ip_addr, ip_addr=ip_addr)
        new_file_buffer = replace_upstream(upstream_pattern)
        make_new_default_file(new_file_buffer)

        shutil.move(SAVE_FILE + "defaults", NGINX_DEFAULT_DIRECTORY + "defaults")
        subprocess.run(["nginx", "-s", "reload"])

    return "action: {action}".format(action=action)


if __name__ == "__main__":
    app.run(host='localhost', port=45000)