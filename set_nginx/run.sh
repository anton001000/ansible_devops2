#!/bin/bash

ansible-playbook --key-file "~/.ssh/id_rsa" provision.yml -i ansible_python_interpreter="/usr/bin/python3"
